package simulator;

import java.util.Vector;

import setting.Properties;
import structs.MotionData;
import structs.CharacterData;
import enumerate.Action;
import enumerate.State;
import simulator.SimAttack;

public class SimCharacter {
	
	private int graphicSizeX;
	private int graphicSizeY;
	private int graphicCenterX;
	private int graphicCenterY;
	
	/**
	 * Hit point, decreased when attacked so it has no minimum value.
	 */
	private int hp;
	/**
	 * Energy, used for a special skill.
	 */
	private int energy;
	/**
	 * The x-coordinate of the character.
	 */
	private int x;
	/**
	 * The y-coordinate of the character.
	 */
	private int y;
	/**
	 * Character's horizontal moving speed.
	 */
	private int speedX;
	/**
	 * Character's vertical moving speed.
	 */
	private int speedY;
	/**
	 * Character's four states: stand, crouch, in-air and down.
	 */
	private State state;
	/**
	 * Character's action.
	 */
	private Action action;
	/**
	 * Attack hit confirm.
	 */
	private boolean hitConfirm;
	/**
	 * Number of attack hits for the current action.
	 */
	private int hitNumber;
	/**
	 * This boolean value indicating the side which the character is facing to.
	 */
	private boolean front;
	/**
	 * According to remainingFrame (and other information),a sign whether or not the character is controllable.
	private boolean control;
	/**
	 * The number of frames that the character needs to resume to its normal status (the current action's number of remaining frames).
	 */
	private int remainingFrame;
	/**
	 * The attack data that the character is using.
	 */
	private SimAttack attack;	
	/**
	 * player's side flag
	 */
	private boolean playerNumber;
	/**
	 * Value of max energy.
	 */
	private int maxEnergy;
	/**
	 * MotionData of this character.
	 * A motion can be accessed by method playerOneMotion.elementAt(index).
	 * ex. playerOneMotion.elementAt(Action.STAND_A.ordinal()).getSpeedX();
	 */
	private Vector<MotionData> motionVector;
	/**
	 * game setting properties
	 */
	private Properties prop;
	
	public SimCharacter(CharacterData characterData, Vector<MotionData> motionData,boolean player){
		this.x = characterData.getX();
		this.y = characterData.getY();
		this.graphicSizeX = characterData.getGraphicSizeX();
		this.graphicSizeY = characterData.getGraphicSizeY();
		this.graphicCenterX = characterData.getGraphicCenterX();
		this.graphicCenterY = characterData.getGraphicCenterY();
		this.hp = characterData.getHp();
		this.energy = characterData.getEnergy();
		this.speedX = characterData.getSpeedX();
		this.speedY = characterData.getSpeedY();
		this.state = characterData.getState();
		this.action = characterData.getAction();
		this.hitConfirm = false;
		this.hitNumber = 0;
		this.front = characterData.isFront();
		this.control = characterData.isControl();
		this.remainingFrame = characterData.getRemainingFrame();
		if(characterData.getAttack() != null)this.attack = new SimAttack(characterData.getAttack());
		else this.attack = null;
		this.playerNumber = player;
		this.maxEnergy = 1000;
		this.motionVector = motionData;
		this.prop = new Properties();
		}
	
	public void runMotion(Action motionNumber){
		if(getAction() != motionNumber){
			setRemainingFrame(motionVector.elementAt(motionNumber.ordinal()).getFrameNumber());
			setHitConfirm(false);
			setAttack(null);
			setHitNumber(0);
			setEnergy(getEnergy()+motionVector.elementAt(action.ordinal()).getAttackStartAddEnergy());
		}
		setAction(motionNumber);
		setState(motionVector.elementAt(motionNumber.ordinal()).getState());
		if(motionVector.elementAt(motionNumber.ordinal()).getSpeedX() != 0){
			if(isFront()){
				setSpeedX(motionVector.elementAt(motionNumber.ordinal()).getSpeedX());
			}
			else{
				setSpeedX(-motionVector.elementAt(motionNumber.ordinal()).getSpeedX());
			}
		}
		setSpeedY(getSpeedY() + motionVector.elementAt(motionNumber.ordinal()).getSpeedY());
		setControl(motionVector.elementAt(motionNumber.ordinal()).isControl());
	}
	
	public void createAttackInstance(){
		if(invokeDecision())
		{	
			SimAttack attack = new SimAttack(motionVector.elementAt(action.ordinal()).getAttackHit(),
					motionVector.elementAt(action.ordinal()).getAttackSpeedX(),motionVector.elementAt(action.ordinal()).getAttackSpeedY(),
					motionVector.elementAt(action.ordinal()).getAttackStartUp(),motionVector.elementAt(action.ordinal()).getAttackInterval(),
					motionVector.elementAt(action.ordinal()).getAttackRepeat(),motionVector.elementAt(action.ordinal()).getAttackActive(),
					motionVector.elementAt(action.ordinal()).getAttackHitDamage(),motionVector.elementAt(action.ordinal()).getAttackGuardDamage(),
					motionVector.elementAt(action.ordinal()).getAttackStartAddEnergy(),motionVector.elementAt(action.ordinal()).getAttackHitAddEnergy(),motionVector.elementAt(action.ordinal()).getAttackGuardAddEnergy(),
					motionVector.elementAt(action.ordinal()).getAttackGiveEnergy(),
					motionVector.elementAt(action.ordinal()).getAttackImpactX(),motionVector.elementAt(action.ordinal()).getAttackImpactY(),
					motionVector.elementAt(action.ordinal()).getAttackGiveGuardRecov(),
					motionVector.elementAt(action.ordinal()).getAttackKnockBack(),
					motionVector.elementAt(action.ordinal()).getAttackHitStop(),
					motionVector.elementAt(action.ordinal()).getAttackType(),
					motionVector.elementAt(action.ordinal()).isAttackDownProperty());
			SimAttack obj = new SimAttack(attack);
			obj.materialise(playerNumber,x,y,getGraphicSizeX(),front);
			setAttack(obj);
		}
	}
	
	public void destroyAttackInstance(){
		setAttack(null);
	}
	
	public void	hitAttack(){
		setHitConfirm(true);
		setHitNumber(getHitNumber()+1);
		setAttack(null);
		//soundEffect[0].play();
	}
	
	public void hitAttackObject(SimCharacter other , SimAttack attackObject){
		boolean guardCheck;
		int hitDirection;

		if((other.getHitAreaL()+other.getHitAreaR())/2 <= (getHitAreaL() + getHitAreaR())/2){
			hitDirection = 1;
		}
		else{
			hitDirection = -1;
		}
		
		switch(getAction()){
		
		case STAND_GUARD:
			if((attackObject.getAttackType() == 1) || (attackObject.getAttackType() == 2)){
				runMotion(Action.STAND_GUARD_RECOV);
				guardCheck = true;
			}
			else guardCheck = false;
			break;
			
		case CROUCH_GUARD:
			if((attackObject.getAttackType() == 1) || (attackObject.getAttackType() == 3)){
				runMotion(Action.CROUCH_GUARD_RECOV);
				guardCheck = true;
			}
		else guardCheck = false;
		break;
		
		case AIR_GUARD:
			if((attackObject.getAttackType() == 1) || (attackObject.getAttackType() == 2)){
				runMotion(Action.AIR_GUARD_RECOV);
				guardCheck = true;
			}
		else guardCheck = false;
		break;
		
		case STAND_GUARD_RECOV:
			runMotion(Action.STAND_GUARD_RECOV);
			guardCheck = true;
			break;
			
		case CROUCH_GUARD_RECOV:
			runMotion(Action.CROUCH_GUARD_RECOV);
			guardCheck = true;
			break;
			
		case AIR_GUARD_RECOV:
			runMotion(Action.AIR_GUARD_RECOV);
			guardCheck = true;
			break;
			
		default:
			guardCheck = false;
			break;
		}
		
		if(guardCheck){
			setHp(getHp() - attackObject.getGuardDamage());
			setEnergy(getEnergy() + attackObject.getGiveEnergy());
			setSpeedX(hitDirection*attackObject.getKnockBack());
			other.setEnergy(other.getEnergy()+attackObject.getGuardAddEnergy());
			setRemainingFrame(attackObject.getGiveGuardRecov());
		}else{
			if((attackObject.getAttackType() == 4)){
				if((getState() != State.AIR) && (this.getState() != State.DOWN)){
					runMotion(Action.THROW_SUFFER);
					other.runMotion(Action.THROW_HIT);
					setHp(getHp() - attackObject.getHitDamage());
					setEnergy(getEnergy() + attackObject.getGiveEnergy());
					other.setEnergy(other.getEnergy()+attackObject.getHitAddEnergy());
				}
			}else{
				setHp(getHp() - attackObject.getHitDamage());
				setEnergy(getEnergy() + attackObject.getGiveEnergy());
				setSpeedX(hitDirection*attackObject.getImpactX());
				setSpeedY(attackObject.getImpactY());
				other.setEnergy(other.getEnergy()+attackObject.getHitAddEnergy());

				if(attackObject.isDownProperty()){
					runMotion(Action.CHANGE_DOWN);
					setRemainingFrame(motionVector.elementAt(action.ordinal()).getFrameNumber());

				}else{
					switch(getState())
					{
					case STAND:
						runMotion(Action.STAND_RECOV);
						break;
					case CROUCH:
						runMotion(Action.CROUCH_RECOV);
						break;
					case AIR:
						runMotion(Action.AIR_RECOV);
						break;
					default:
						break;

					}
				}
			}
		}
	}

	public void frontDecision(int selfX,int otherX){
		if(isFront()){
			if(selfX<otherX){
				setFront(true);
			}
			else{
				setX(getX()-getGraphicSizeX()+getGraphicCenterX()*2);
				setFront(false);
			}
		}
		else{
			if(selfX<otherX){
				setX(getX()+getGraphicSizeX()-getGraphicCenterX()*2);
				setFront(true);
			}
			else{
				setFront(false);
			}
		}
	}
	
	public boolean invokeDecision(){
		if((getMotionVector().elementAt(getAction().ordinal()).getFrameNumber() - getMotionVector().elementAt(getAction().ordinal()).getAttackStartUp()) == getRemainingFrame()) return true;
		else return false;
	}
	
	public boolean durationDecision(){
		if((getMotionVector().elementAt(getAction().ordinal()).getFrameNumber() - getMotionVector().elementAt(getAction().ordinal()).getAttackActive()) >= getRemainingFrame()) return true;
		else return false;
	}
	
	public void update(){
		moveX(getSpeedX());
		moveY(getSpeedY());
		
		frictionEffect();
		gravityEffect();
		
		setRemainingFrame(getRemainingFrame()-1);
		
		if(getEnergy() > getMaxEnergy()) setEnergy(getMaxEnergy());
		
		if(this.getHitAreaB() >= 640){
			if(motionVector.elementAt(getAction().ordinal()).isLandingFlag()){
				runMotion(Action.LANDING);
				setSpeedY(0);
			}
			moveY(640-this.getHitAreaB());
		}
		
		createAttackInstance();
		
		if(getRemainingFrame() <= 0){
			if(getAction() == Action.CHANGE_DOWN){
				runMotion(Action.DOWN);
			}
			else if(getAction() == Action.DOWN){
				runMotion(Action.RISE);
			}
			else if(getState() == State.AIR || getY() < 320){
				runMotion(Action.AIR);
			}
			else{
				runMotion(Action.STAND);
			}
		}
	}
	
	public void moveX(int relativePosition){
		setX(getX() + relativePosition);
	}
	
	public void moveY(int relativePosition){
		setY(getY() + relativePosition);		
	}
	
	public void frictionEffect(){
		if(!(getHitAreaB() < 640)){
			if(getSpeedX() > 0){
				setSpeedX(getSpeedX()-prop.FRICTION);
			}
			else if(getSpeedX() < 0){
				setSpeedX(getSpeedX()+prop.FRICTION);
			}
		}
	}

	public void gravityEffect(){
		if(getHitAreaB() >= 640){
			setSpeedY(0);
		}
		else
		{
			setSpeedY(getSpeedY()+prop.GRAVITY);
		}
	}
	
	/**
	 * Set the character's hp.
	 * 
	 * @param int The character's hp.
	 */
	private void setHp(int hp) {
		this.hp = hp;
	}
	/**
	 * Set the character's energy.
	 * 
	 * @param int The character's energy.
	 */
	private void setEnergy(int energy) {
		this.energy = energy;
	}
	/**
	 * Set the character box's most-left x-coordinate.
	 * @param int The character box's x-coordinate.
	 */
	private void setX(int x) {
		this.x = x;
	}
	/**
	 * Set the character box's most-top y-coordinate.
	 * @param int The character box's y-coordinate.
	 */
	private void setY(int y) {
		this.y = y;
	}
	/**
	 * Set the character's horizontal speed.
	 * @param int The character's horizontal speed.
	 */
	private void setSpeedX(int speedX) {
		this.speedX = speedX;
	}
	public void reversalSpeedX(){
		this.speedX = -(this.speedX/2);
	}
	/**
	 * @return The character's vertical speed.
	 */
	private void setSpeedY(int speedY) {
		this.speedY = speedY;
	}
	/**
	 * Set the character's state: stand / crouch / in air / down.
	 * 
	 * @param State The character's state: stand / crouch / in air / down.
	 */
	private void setState(State state) {
		this.state = state;
	}
	/**
	 * Set the character's action.
	 * 
	 * @param Action The character's action.
	 */
	private void setAction(Action action) {
		this.action = action;
	}
	public void setHitConfirm(boolean hitConfirm) {
		this.hitConfirm = hitConfirm;
	}
	/**
	 * Set the character's facing direction.
	 * 
	 * @param front boolean The character's facing direction.
	 */
	public void setFront(boolean front) {
		this.front = front;
	}
	/**
	 * Set the flag whether this character can run a motion with a command.
	 * 
	 * @param boolean 
	 */
	private void setControl(boolean control) {
		this.control = control;
	}
	/**
	 * Set the number of frames that the character takes to resume to its normal status (the current action's number of remaining frames). 
	 * 
	 * @param int The number of frames that the character takes to resume to its normal status.
	 */
	private void setRemainingFrame(int remainingFrame) {
		this.remainingFrame = remainingFrame;
	}
	
	/**
	 * @return The character's hp.
	 */
	public int getHp() {
		return hp;
	}
	/**
	 * @return The character's energy.
	 */
	public int getEnergy() {
		return energy;
	}
	/**
	 * @return The character box's most-left x-coordinate.
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return The character box's most-top y-coordinate.
	 */
	public int getY() {
		return y;
	}
	/**
	 * @return The character's horizontal speed.
	 */
	public int getSpeedX() {
		return speedX;
	}
	/**
	 * @return The character's vertical speed.
	 */
	public int getSpeedY() {
		return speedY;
	}
	/**
	 * @return The character's state: stand / crouch / in air / down.
	 */
	public State getState() {
		return state;
	}
	/**
	 * @return The character's action.
	 */
	public Action getAction() {
		return action;
	}
	/**
	 * @return Attack hit confirm.
	 */
	public boolean isHitConfirm(){
		return hitConfirm;
	}
	
	/**
	 * @return The number of attack hits for the current action.
	 */
	public int getHitNumber() {
		return hitNumber;
	}

	/**
	 * @param hitNumber int Number of attack hits for the current action.
	 */
	public void setHitNumber(int hitNumber) {
		this.hitNumber = hitNumber;
	}

	/**
	 * @return The character's facing direction.
	 */
	public boolean isFront() {
		return front;
	}
	/**
	 * @return The flag whether this character can run a motion with a command.
	 */
	public boolean isControl() {
		return control;
	}
	/**
	 * @return The number of frames that the character needs to resume to its normal status.
	 */
	public int getRemainingFrame() {
		return remainingFrame;
	}

	/**
	 * @return player side's flag
	 */
	public boolean isPlayerNumber() {
		return playerNumber;
	}
	
	/**
	 * @return Value of max energy.
	 */
	public int getMaxEnergy() {
		return maxEnergy;
	}
	
	/**
	 * @return The character's hit box's most-right x-coordinate.
	 */
	public int getHitAreaR(){
		if(this.isFront())
		{
			return motionVector.elementAt((this.getAction()).ordinal()).getHit().getR() + x;
		}
		else
		{
			return getGraphicSizeX() - motionVector.elementAt((this.getAction()).ordinal()).getHit().getL() + x;
		}
	}
	
	/**
	 * @return The character's hit box's most-left x-coordinate.
	 */
	public int getHitAreaL(){
		if(this.isFront())
		{
			return motionVector.elementAt((this.getAction()).ordinal()).getHit().getL() + x;
		}
		else
		{
			return getGraphicSizeX() - motionVector.elementAt((this.getAction()).ordinal()).getHit().getR() + x;
		}
	}
	
	/**
	 * @return The character's hit box's most-top y-coordinate.
	 */
	public int getHitAreaT(){
		return motionVector.elementAt((this.getAction()).ordinal()).getHit().getT() + y;		
	}
	
	/**
	 * @return The character's hit box's most-bottom y-coordinate.
	 */
	public int getHitAreaB(){
		return motionVector.elementAt((this.getAction()).ordinal()).getHit().getB() + y;		
	}

	/**
	 * MotionData of the character.
	 * A motion can be accessed by method playerOneMotion.elementAt(index).
	 * @return Motion data of this character
	 */
	public Vector<MotionData> getMotionVector() {
		return motionVector;
	}

	/**
	 * @return The attack data that the character is using.
	 */
	public SimAttack getAttack() {
		return attack;
	}

	/**
	 * Set the attack data that the character is using.
	 * 
	 * @param attack The attack data that the character is using.
	 */
	public void setAttack(SimAttack attack) {
		if(attack != null){
			SimAttack temp = new SimAttack(attack);
			this.attack = temp;
		}
		else{
			this.attack = null;
		}
	}

	public int getGraphicSizeX(){
		return graphicSizeX;
	}
	
	public int getGraphicSizeY(){
		return graphicSizeY;
	}

	public int getGraphicCenterX() {
		return graphicCenterX;
	}

	public void setGraphicCenterX(int centerX) {
		this.graphicCenterX = centerX;
	}

	public int getGraphicCenterY() {
		return graphicCenterY;
	}

	public void setGraphicCenterY(int centerY) {
		this.graphicCenterY = centerY;
	}

	public void setGraphicSizeX(int sizeX) {
		this.graphicSizeX = sizeX;
	}

	public void setGraphicSizeY(int sizeY) {
		this.graphicSizeY = sizeY;
	}

	
}
