import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.commons.io.FileUtils;

public class Selection {
	private static String directoryPath = "./log/point";
	private static int dataSize=2;
	private static int generation=0;

	public static void fitnessFunction() throws IOException {

		int gaScore[][] = new int[3 * RandomGenerator.noMatch][4];
		int temp[] = new int[4];

		File dir = new File(directoryPath);
		File[] files = dir.listFiles();
		int count = 0;

		for (File file : files) {
			if (!file.exists()) {
				continue;
			} else if (file.isFile()) {
				try (BufferedReader br = new BufferedReader(new FileReader(file))) {
					String line;
					int j = count * 3;
					while ((line = br.readLine()) != null) {
						if (Character.isDigit(line.charAt(0)) && Character.isDigit(line.charAt(line.length() - 1))) {
							String[] parts = line.split(",");
							temp[0] = Integer.parseInt(parts[0]);
							temp[1] = Integer.parseInt(parts[1]);
							temp[2] = Integer.parseInt(parts[2]);
							// time not in use
							temp[3] = Integer.parseInt(parts[3]);
							gaScore[j] = temp.clone();
							j++;
						} else {
							count--;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			count++;
		}

		for (int i = 0; i < 3 * RandomGenerator.noMatch; i++) {
			int score = 1000 * gaScore[i][1] / (gaScore[i][1] + gaScore[i][2]);
			int myHP = gaScore[i][1];
			int oppHP = gaScore[i][2];

			// temp file will delete during selection
			File thisGenerationScore = new File("thisGenerationScore.txt");
			try (FileWriter fw1 = new FileWriter(thisGenerationScore, true);
					BufferedWriter bw1 = new BufferedWriter(fw1);
					PrintWriter out1 = new PrintWriter(bw1);) {
				out1.println(Integer.toString(score) + "," + Integer.toString(myHP) + "," + Integer.toString(oppHP));
				out1.close();
			} catch (IOException e) {
				System.out.println("COULD NOT LOG!!");
			}

			// final evidence
			File allGenerationScore = new File("allGenerationScore.txt");
			try (FileWriter fw2 = new FileWriter(allGenerationScore, true);
					BufferedWriter bw2 = new BufferedWriter(fw2);
					PrintWriter out2 = new PrintWriter(bw2);) {
				out2.println(Integer.toString(score) + "," + Integer.toString(myHP) + "," + Integer.toString(oppHP));
				out2.close();
			} catch (IOException e) {
				System.out.println("COULD NOT LOG!!");
			}
		}

		FileUtils.cleanDirectory(dir);

	}

	public static void initSelection() {
		String path1 = "thisGenerationScore.txt";
		int resultDetail[][] = new int[3 * RandomGenerator.noMatch][3];
		int currentBest1[] = new int[dataSize];
		int concludetemp[] = new int[dataSize];
		int tempResult[] = new int[3];

		try (BufferedReader br = new BufferedReader(new FileReader(path1))) {
			String line;
			// j is number of rounds
			int j = 0;
			// used to index current AI
			int k = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\\,");
				tempResult[0] = Integer.parseInt(parts[0]);
				tempResult[1] = Integer.parseInt(parts[1]);
				tempResult[2] = Integer.parseInt(parts[2]);

				resultDetail[j] = tempResult.clone();
				j++;
				if (j == 3 * RandomGenerator.noMatch) {
					for (int i = 0; i < 3 * RandomGenerator.noMatch; i++) {
						concludetemp[0] += resultDetail[i][0];
					}

					// index of which AI
					concludetemp[1] = k;

					if (concludetemp[0] > 2000 * RandomGenerator.noMatch) {
						currentBest1 = concludetemp.clone();
					}

					j = 0;
					k++;
					for (int i = 0; i < dataSize; i++) {
						concludetemp[i] = 0;
					}
				}
			}

			if (currentBest1[0] > 2000 * RandomGenerator.noMatch) {
				RandomGenerator.bestActionChro1 = RandomGenerator.actNamePopulation[currentBest1[1]].clone();
				RandomGenerator.corresProbChro1 = RandomGenerator.ActProbPopulation[currentBest1[1]].clone();
				currentBest1[1] = RandomGenerator.testingPopSize;
				RandomGenerator.bestScore1 = currentBest1.clone();
				System.out.println(generation);
				System.out.println(RandomGenerator.bestScore1[0]);
				//When score over 2000*nomatch, we record this.
				File bestChromosome1 = new File("bestChromosome1.txt");
				try (FileWriter fw = new FileWriter(bestChromosome1, true);
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw);) {

					for (int i = 0; i < RandomGenerator.chromLength; i++) {
						out.println(RandomGenerator.bestActionChro1[i]);
					}

					for (int i = 0; i < RandomGenerator.chromLength; i++) {
						out.println(RandomGenerator.corresProbChro1[i]);
					}

					out.close();
				} catch (IOException e) {
					System.out.println("COULD NOT LOG!!");
				}
			}else{
				generation++;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		File f = null;
		boolean bool = false;
		try {
			f = new File("thisGenerationScore.txt");
			bool = f.delete();
			f.createNewFile();
			bool = f.delete();

		} catch (Exception e) {
			// if any error occurs
			e.printStackTrace();
		}
	}

	public static void generationSelection() {
		String path1 = "thisGenerationScore.txt";
		int resultDetail[][] = new int[3 * RandomGenerator.noMatch][3];
		int currentBest1[] = new int[dataSize];
		// copy the best two so far
		currentBest1 = RandomGenerator.bestScore1.clone();
		int concludetemp[] = new int[dataSize];
		int tempResult[] = new int[3];

		try (BufferedReader br = new BufferedReader(new FileReader(path1))) {
			String line;
			// j is number of rounds
			int j = 0;
			// used to index current AI
			int k = 0;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\\,");
				tempResult[0] = Integer.parseInt(parts[0]);
				tempResult[1] = Integer.parseInt(parts[1]);
				tempResult[2] = Integer.parseInt(parts[2]);
				resultDetail[j] = tempResult.clone();
				j++;

				// one AI finished, conclude the data
				// compare with parents
				if (j == 3 * RandomGenerator.noMatch) {
					for (int i = 0; i < 3 * RandomGenerator.noMatch; i++) {
						concludetemp[0] = resultDetail[i][0] + concludetemp[0];
					}
					
					// index of which AI
					concludetemp[1] = k;

					if (concludetemp[0] > 2000 * RandomGenerator.noMatch) {
						currentBest1 = concludetemp.clone();
					}

					j = 0;
					k++;
					for (int i = 0; i < dataSize; i++) {
						concludetemp[i] = 0;
					}
				}
			}

			if (currentBest1[0] > 2000 * RandomGenerator.noMatch) {
				RandomGenerator.bestActionChro1 = RandomGenerator.actNamePopulation[currentBest1[1]].clone();
				RandomGenerator.corresProbChro1 = RandomGenerator.ActProbPopulation[currentBest1[1]].clone();
				currentBest1[1] = RandomGenerator.testingPopSize;
				RandomGenerator.bestScore1 = currentBest1.clone();
				System.out.println(generation);
				System.out.println(RandomGenerator.bestScore1[0]);
				//When score over 2000*nomatch, we record this.
				File bestChromosome1 = new File("bestChromosome1.txt");
				try (FileWriter fw = new FileWriter(bestChromosome1, true);
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw);) {

					for (int i = 0; i < RandomGenerator.chromLength; i++) {
						out.println(RandomGenerator.bestActionChro1[i]);
					}

					for (int i = 0; i < RandomGenerator.chromLength; i++) {
						out.println(RandomGenerator.corresProbChro1[i]);
					}

					out.close();
				} catch (IOException e) {
					System.out.println("COULD NOT LOG!!");
				}
			}else{
				generation++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		File f = null;
		boolean bool = false;
		try {
			f = new File("thisGenerationScore.txt");
			bool = f.delete();
			f.createNewFile();
			bool = f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}